// vue.config.js
module.exports = {
    // options...
    pages: {
        index: {
            entry: "src/app.js",
            template: "public/index.html",
            filename: "index.html"
        }
    },
    baseUrl: "./",
    devServer: {
        open: true
    }
  }