import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    skills: ["C#", "Vue.js", "React", "JavaScript", "HTML"]
  },
  actions: {
    addSkill: (context, skill) => 
      context.commit("addSkill", skill)
    ,
    removeSkill: (context, skillIndex) => 
      context.commit("removeSkill", skillIndex)
  },
  mutations: {
    addSkill: (state, skill ) => {
      state.skills.push(skill);
    },
    removeSkill: (state, skillIndex ) => {
      state.skills.splice(skillIndex, 1);
    }
  },
  // Getters are used to compute the internal state and return custom data
  // Components have property style access to them, store.getters.getterName
  getters: {}
});
