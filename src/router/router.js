import Vue from "vue";
import Router from "vue-router";
import Skills from "../components/skills/Skills.vue";
import About from "../components/about/About.vue";

import Samples from "../components/samples/Samples.vue";
import DataBinding from "../components/samples/dataBinding/DataBinding.vue";
import Drums from "../components/samples/drums/Drums.vue";
import CssVariables from "../components/samples/cssVariables/CssVariables.vue";

import NotFound from "../components/notFound/NotFound.vue";

Vue.use(Router);

export default new Router({
  // The default mode for the router is hash mode.  It uses the URL hash to simulate a full URL
  // so that the page won't be reloaded when the URL changes
  // history mode -- When using history mode, the URL will look "normal," e.g. http://oursite.com/user/id.
  // However, for history mode it is mandatory for the app to be hosted on a web server and not be a statically
  // opened file == file:///
  mode: "hash",
  routes: [
    {
      path: "/",
      name: "skills",
      component: Skills
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/samples/:name?",
      name: "samples",
      component: Samples,
      children: [
        { path: "dataBinding", component: DataBinding },
        { path: "drums", component: Drums },
        { path: "cssVariables", component: CssVariables },
      ]
    },
    {
      path: "*",
      name: "notFound",
      component: NotFound
    }
  ]
  // Applies the css class .active declared in app.css to links that match the currently active route
  // linkExactActiveClass: "active"
});
