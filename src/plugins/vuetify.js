import Vue from "vue";
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VTextField,
  VDivider,
  transitions
} from "vuetify";
import "vuetify/src/stylus/app.styl";

// The A la carte system enables you to pick and choose which
// components to import, drastically lowering your build size

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VTextField,
    VDivider,
    transitions
  }
});
